<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('services', function (Blueprint $table) {

            $table->id("service_id");
            $table->string("service_title");
            $table->unsignedBigInteger('service_account_id');
            $table->unsignedBigInteger('service_category_id');
            $table->unsignedBigInteger('service_town_id');

            $table->foreign('service_account_id')->references('account_id')->on('accounts')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign('service_category_id')->references('category_id')->on('categories')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign('service_town_id')->references('town_id')->on('towns')->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('services');
    }
};
