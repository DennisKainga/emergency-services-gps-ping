<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->id("account_id");
            $table->string("account_first_name");
            $table->string("account_last_name");
            $table->string("account_telephone_number");
            $table->unsignedBigInteger("account_login_id");
            $table->foreign("account_login_id")->references("login_id")->on("login")->cascadeOnDelete()->cascadeOnUpdate();
            $table->unsignedBigInteger("account_town_id");
            $table->foreign("account_town_id")->references("town_id")->on("towns")->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('accounts');
    }
};
