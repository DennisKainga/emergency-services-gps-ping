<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('towns', function (Blueprint $table) {
            $table->id("town_id");
            $table->string("town_name");
            $table->double("town_longitude");
            $table->double("town_latitude");
            $table->unsignedBigInteger('town_county_id');
            $table->foreign('town_county_id')->references('county_id')->on('counties')->cascadeOnDelete()->cascadeOnUpdate();
           
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('towns');
    }
};
