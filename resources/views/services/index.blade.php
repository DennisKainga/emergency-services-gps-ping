<x-Layout title="Services">

    <div class="row">
        @unless (sizeof($services) == 0)

            {{-- // 'service_title',
            // 'service_account_id',
            // 'service_category_id',
            // 'service_town_id' --}}

            @foreach ($services as $service)
                {{-- show each town in a card --}}

                <x-TextCard title="{{ $service->service_title }}" longitude="{{ $service->town->town_name }}"
                    latitude="{{ $service->town->county->county_name }}" id="{{ $service->service_id }}"
                    contact="{{ $service->account->account_telephone_number }}"
                    county="{{ $service->account->account_first_name . ' ' . $service->account->account_last_name }}">
                </x-TextCard>

                {{-- show edit form --}}
                <x-Service.Form title="{{ 'Edit ' . $service->service_title . ' Service' }}"
                    modal="{{ 'edit' . $service->service_id }}" :service="$service" :supervisors="$supervisors" :categories="$categories"
                    :towns="$towns" submit_route="{{ route('service-update', $service->service_id) }}">
                </x-Service.Form>


                {{-- Show Delete Modal --}}
                <x-DeleteModal title="{{ 'Delete' . ' ' . $service->service_title . ' Service' }}"
                    modal="{{ 'delete' . $service->service_id }}"
                    delete_route="{{ route('service-delete', $service->service_id) }}">
                </x-DeleteModal>
            @endforeach
        @endunless
    </div>

    {{-- show create form --}}
    <x-Service.Form title="New Service" modal="add-new" :categories="$categories" :supervisors="$supervisors" :towns="$towns"
        submit_route="{{ route('service-store') }}">
    </x-Service.Form>



    {{-- <table class="table d-none" id="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Garage Name</th>
                <th scope="col">Garage owner</th>
                <th scope="col">County Location</th>
                <th scope="col">Town Location</th>
                <th scope="col">Longitude</th>
                <th scope="col">Latitude</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($garages as $i => $service)
                <tr>
                    <th scope="row">{{ $i + 1 }}</th>
                    <td>{{ $service->garage_title }}</td>
                    <td>{{ $service->town->county->county_name }}</td>
                    <td>{{ $service->account->account_first_name . ' ' . $service->account->account_last_name }}</td>
                    <td>{{ $service->town->town_name }}</td>
                    <td>{{ $service->town->town_longitude }}</td>
                    <td>{{ $service->town->town_latitude }}</td>


                </tr>
            @endforeach

        </tbody>
    </table> --}}

</x-Layout>
