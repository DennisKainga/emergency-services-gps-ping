@props(["title","modal","submit_route","county"=>null])
<x-Modal title="{{ $title }}" modal="{{ $modal }}"  >
    <form method="POST" action="{{ $submit_route }}">
        @csrf
        @if($county == null)
        @method("POST")
        @else
        @method("PUT")
        @endif
        <div class="form-group">
          <label for="inputAddress">County Name</label>
          <input value="{{ $county ? $county->county_name : '' }}"  type="text" name="county_name" class="form-control" placeholder="Nairobi">
        </div>                  
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
</x-Modal>

