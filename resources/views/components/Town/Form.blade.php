@props(["title","modal","counties","town"=>null,"submit_route"])

<x-Modal title="{{ $title }}" modal="{{ $modal }}">
    
    <form method="POST" action="{{ $submit_route }}">
        @csrf
        @if($town == null)
        @method("POST")
        @else
        @method("PUT")
        @endif
        <div class="form-group">
            <label for="inputAddress">Town Name</label>
            <input value="{{ $town ? $town->town_name : '' }}"  type="text" name="town_name" class="form-control" placeholder="Limuru">
        </div>    
    
        <div class="form-row align-items-center">
            <div class="col-12 my-1">
                <label class="mr-sm-2" for="inlineFormCustomSelect">County</label>
                <select class="custom-select mr-sm-2" name="county_name">

                    @if($town !=null)
                        <option selected value="{{ $town->town_county_id }}">{{ $town->county->county_name }}</option>
                    @else
                        <option>Choose...</option>
                    @endif
                    @foreach ($counties as $county)
                        <option value="{{ $county->county_id }}">{{ $county->county_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </form>
</x-Modal>

