@auth('admin')
    @php
        $routes = [
            [
                'route_name' => 'Supervisors',
                'route_url' => 'accounts/supervisors',
            ],
            [
                'route_name' => 'cutomers',
                'route_url' => 'accounts/customers',
            ],
            [
                'route_name' => 'counties',
                'route_url' => 'counties',
            ],
            [
                'route_name' => 'towns',
                'route_url' => 'towns',
            ],
            [
                'route_name' => 'Services',
                'route_url' => 'services',
            ],
        ];
    @endphp
@endauth
@auth('customer')
    @php
        $routes = [];
    @endphp
@endauth
@props(['title'])

<x-BaseLayout>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fa fa-bars"></span> Menu
            </button>
            <form action="#" class="searchform order-lg-last">
                <div class="form-group d-flex">
                    <input type="text" class="form-control pl-3" placeholder="Search">
                    <button type="submit" placeholder="" class="form-control search"><span
                            class="fa fa-search"></span></button>
                </div>
            </form>
            <div class="collapse navbar-collapse" id="ftco-nav">
                <ul class="navbar-nav mr-auto">
                    @foreach ($routes as $route)
                        <li class="nav-item"><a href="/{{ $route['route_url'] }}"
                                class="nav-link">{{ $route['route_name'] }}</a></li>
                    @endforeach
                    <li class="nav-item"><a href="{{ route('auth-logout') }}" class="nav-link">Log out</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- END nav -->

    <section class="hero-wrap hero-wrap-2" style="background-image: url(
        {{ asset('images/bg_2.jpg') }})"
        data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-end">
                <div class="col-md-9 ftco-animate pb-5">
                    <p class="breadcrumbs mb-2"><span class="mr-2"><a href="/">Home <i
                                    class="fa fa-chevron-right"></i></a></span> <span>{{ $title }} <i
                                class="fa fa-chevron-right"></i></span></p>
                    <h1 class="mb-0 bread">{{ $title }}</h1>
                </div>
            </div>
        </div>
    </section>


    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center pb-5 mb-3">
                <div class="col-md-7 heading-section text-center ftco-animate">
                    <span class="subheading">{{ $title }}</span>
                    <h2>{{ $title }}</h2>
                </div>

            </div>
            @auth('admin')
                @if (request()->segment(1) != 'dashboard')
                    <button type="button" id="pdf-button" class="btn btn-secondary text-light mb-3 ml-3">Pdf</button>
                    <button class="btn btn-success mb-3" data-toggle="modal" data-target="#add-new">New
                        {{ $title }}</button>
                @endif
            @endauth

            @auth('customer')
                <button class="btn btn-success mb-3" data-toggle="modal" data-target="#ping-location">Ping Location</button>
            @endauth



            {{ $slot }}
        </div>
    </section>


    <footer class="footer ftco-section">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-6 col-lg">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="logo"><a href="#">Emergency Services<span>.</span></a></h2>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                            there live the blind texts.</p>
                        <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-4">
                            <li class="ftco-animate"><a href="#"><span class="fa fa-twitter"></span></a></li>
                            <li class="ftco-animate"><a href="#"><span class="fa fa-facebook"></span></a></li>
                            <li class="ftco-animate"><a href="#"><span class="fa fa-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg">
                    <div class="ftco-footer-widget mb-4 ml-md-5">
                        <h2 class="ftco-heading-2">Services</h2>
                        <ul class="list-unstyled">
                            <li><a href="#" class="py-1 d-block"><span class="fa fa-check mr-3"></span>Fire
                                    Trucks</a></li>

                            <li><a href="#" class="py-1 d-block"><span class="fa fa-check mr-3"></span>Tow
                                    Truck</a></li>
                            <li><a href="#" class="py-1 d-block"><span class="fa fa-check mr-3"></span>Police</a>
                            </li>
                            <li><a href="#" class="py-1 d-block"><span class="fa fa-check mr-3"></span>EMTs</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Contact information</h2>
                        <div class="block-23 mb-3">
                            <ul>
                                <li><span class="icon fa fa-map-marker"></span><span class="text">203 Fake St.
                                        Mountain View, San Francisco, California, USA</span></li>
                                <li><a href="#"><span class="icon fa fa-phone"></span><span class="text">+2
                                            392
                                            3929 210</span></a></li>
                                <li><a href="#"><span class="icon fa fa-paper-plane"></span><span
                                            class="text">info@yourdomain.com</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Business Hours</h2>
                        <div class="opening-hours">
                            <h4>Opening Days:</h4>
                            <p class="pl-3">
                                <span>Monday – Friday : 9am to 20 pm</span>
                                <span>Saturday : 9am to 17 pm</span>
                            </p>
                            <h4>Vacations:</h4>
                            <p class="pl-3">
                                <span>All Sunday Days</span>
                                <span>All Official Holidays</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;
                        <script>
                            document.write(new Date().getFullYear());
                        </script> My 4th Year Project</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4"
                stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4"
                stroke-miterlimit="10" stroke="#F96D00" />
        </svg></div>
    <x-Modal title="Pinging location" modal="ping-location">
        <p class="text-danger">This will fetch mechanics nearby</p>
        <form action="{{ route('location-ping') }}" method="POST">

            @csrf
            <div class="form-group">
                <input type="text" name="latitude" id="latitude">
                <input type="text" name="longitude" id="longitude">
            </div>

            <div class="d-flex justify-content-between">

                <button class="btn btn-success btn-sm" data-dismiss="modal">Cancel</button>
                <button class="btn btn-info btn-sm">Proceed</button>
            </div>
        </form>
    </x-Modal>

</x-BaseLayout>
<script>
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
    } else {
        alert("Geolocation is not supported by this browser.");
    }

    function successCallback(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;

        // Use the GPS coordinates
        // ...
        // Prefill the input fields
        document.getElementById("latitude").value = latitude;
        document.getElementById("longitude").value = longitude;
        //   console.log(latitude +" ,"+longitude);
    }

    function errorCallback(error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                alert("User denied the request for Geolocation.");
                break;
            case error.POSITION_UNAVAILABLE:
                alert("Location information is unavailable.");
                break;
            case error.TIMEOUT:
                alert("The request to get user location timed out.");
                break;
            case error.UNKNOWN_ERROR:
                alert("An unknown error occurred.");
                break;
        }
    }
</script>
<script>
    const pdfButton = document.getElementById('pdf-button');
    const table = document.getElementById('table');

    pdfButton.addEventListener('click', () => {
        const doc = new jsPDF();

        // Convert the table to a PDF using jsPDF's autoTable plugin
        doc.autoTable({
            html: table
        });

        // Save the PDF

        doc.save('report.pdf');
    });
</script>
