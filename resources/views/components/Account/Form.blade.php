@props(['title', 'submit_route', 'modal', 'towns', 'account' => null])
<x-Modal title="{{ $title }}" modal="{{ $modal }}">

    <form method="POST" action="{{ $submit_route }}">
        @csrf
        @if ($account == null)
            @method('POST')
        @else
            @method('PUT')
        @endif
        <input type="hidden" name="login_rank" value="{{ request()->segment(2) === 'supervisors' ? 0 : 1 }}">


        <div class="form-row align-items-center">
            <div class="col-6 my-1">
                <label class="mr-sm-2" for="inlineFormCustomSelect">First Name</label>
                <input value="{{ $account ? $account->account_first_name : '' }}" type="text"
                    name="account_first_name" class="form-control" placeholder="Kim">

                {{-- <input value="{{ $account->account_first_name   }}"  type="text" name="account_first_name" class="form-control"> --}}
                </select>
            </div>

            <div class="col-6 my-1">
                <label class="mr-sm-2" for="inlineFormCustomSelect">Last Name</label>
                <input value="{{ $account ? $account->account_last_name : '' }}" type="text" name="account_last_name"
                    class="form-control" placeholder="Marcelo">
            </div>
        </div>

        <div class="form-group">
            <label for="inputAddress">Contact</label>
            <input value="{{ $account ? $account->account_telephone_number : '' }}" type="text"
                name="account_telephone_number" class="form-control" placeholder="0776576464">
        </div>

        <div class="form-group">
            <label for="inputAddress">Email</label>
            <input value="{{ $account ? $account->login->login_email : '' }}" type="email" name="login_email"
                class="form-control" placeholder="doe@gmail.com">
        </div>

        <div class="form-row align-items-center">
            <div class="col-12 my-1">
                <label class="mr-sm-2" for="inlineFormCustomSelect">Town (Location)</label>
                <select class="custom-select mr-sm-2" name="account_town">
                    @if ($account != null)
                        <option value="{{ $account->account_town_id }}" selected>{{ $account->town->town_name }}
                        </option>
                    @else
                        <option selected>Choose...</option>
                    @endif
                    @foreach ($towns as $town)
                        <option value="{{ $town->town_id }}">{{ $town->town_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
</x-Modal>
