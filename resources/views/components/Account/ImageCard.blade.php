@props(["user"])
<div class="col-md-6 col-lg-3 ftco-animate">
    <div class="staff">
        <div class="img-wrap d-flex align-items-stretch">
            <div class="img align-self-stretch" style="background-image: url(
                {{asset('images/mechanic.jpg')}});
                "></div>
        </div>
        <div class="text pt-3 px-3 pb-4 text-center">
            <h3 class="text-capitalize">{{ $user->account_first_name.' '.$user->account_last_name }}</h3>
            <span class="position mb-2 text-lowercase">{{ $user->login->login_email }}</span>
            <span class="position mb-2 text-capitalize">{{  $user->town->county->county_name.','.$user->town->town_name }}</span>
            <span class="position mb-2 text-capitalize">{{ $user->account_telephone_number }}</span>
            <div class="faded">
                {{-- <p>I am an ambitious workaholic, but apart from that, pretty simple person.</p> --}}
                <ul class="ftco-social text-center">
                    <li class="ftco-animate"><a href="#!" data-toggle="modal" data-target="{{'#edit'.$user->account_id}}"  class="d-flex align-items-center justify-content-center"><span class="fa fa-edit"></span></a></li>
                    <li class="ftco-animate"><a href="#"  data-toggle="modal" data-target="{{'#delete'.$user->account_id}}"  class="d-flex align-items-center justify-content-center"><span class="fa fa-trash"></span></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>