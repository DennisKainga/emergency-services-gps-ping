@props(["id","title","longitude","latitude","county"=>null,"contact"=>null])

{{-- Pass id to uniquely id every modal target --}}

<div class="col-md-4 services ftco-animate">
    <div class="d-block d-flex">
      <div class="icon d-flex justify-content-center align-items-center">
        <span class="flaticon-car-service"> <img src="" alt="" srcset=""></span>
      </div>
      <div class="media-body pl-3">
        <h3 class="heading">{{ $title }}</h3>
        @unless (!$county)
          <h5 class="heading">{{ $county }}</h5>
          @auth('customer')
            <a href="tel:+{{ $contact }}"><h6 class="heading">{{ $contact }}</h6></a>
          @endauth
        @endunless
        <p>{{$latitude.' , '.$longitude}}</p>
        
        @auth('admin')
          <div class="d-flex justify-content-between">
            <p><a href="#!" data-toggle="modal" data-target="{{'#edit'.$id}}"  class="btn-custom">Edit</a></p>
            <p><a href="#!" data-toggle="modal" data-target="{{'#delete'.$id}}"  class="btn-custom">Delete</a></p>
          </div>
        @endauth
    </div>
    </div>
</div>