@props(['title', 'modal', 'categories', 'towns', 'supervisors', 'service' => null, 'submit_route'])

<x-Modal title="{{ $title }}" modal="{{ $modal }}">

    <form method="POST" action="{{ $submit_route }}">

        @csrf
        @if ($service == null)
            @method('POST')
        @else
            @method('PUT')
        @endif
        <div class="form-group">
            <label for="inputAddress">Service Name</label>
            <input value="{{ $service ? $service->service_title : '' }}" type="text" name="service_title"
                class="form-control" placeholder="Marcelo">
        </div>

        <div class="form-row align-items-center">
            <div class="col-12 my-1">
                <label class="mr-sm-2" for="inlineFormCustomSelect">Town (Location)</label>
                <select class="custom-select mr-sm-2" name="service_town_id">
                    @if ($service != null)
                        <option selected value="{{ $service->service_town_id }}">{{ $service->town->town_name }}
                        </option>
                    @else
                        <option>Choose...</option>
                    @endif
                    @foreach ($towns as $town)
                        <option value="{{ $town->town_id }}">{{ $town->town_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-row align-items-center">
            <div class="col-12 my-1">
                <label class="mr-sm-2" for="inlineFormCustomSelect">Supervisor</label>
                <select class="custom-select mr-sm-2" name="service_account_id">
                    @if ($service != null)
                        <option selected value="{{ $service->service_account_id }}">
                            {{ $service->account->account_first_name . ' ' . $service->account->account_last_name }}
                        </option>
                    @else
                        <option>Choose...</option>
                    @endif

                    @foreach ($supervisors as $supervisor)
                        <option value="{{ $supervisor->account_id }}">
                            {{ $supervisor->account_first_name . ' ' . $supervisor->account_last_name }}</option>
                    @endforeach


                </select>
            </div>
        </div>
        <div class="form-row align-items-center">
            <div class="col-12 my-1">
                <label class="mr-sm-2" for="inlineFormCustomSelect">Category</label>
                <select class="custom-select mr-sm-2" name="service_category_id">
                    @if ($service != null)
                        <option selected value="{{ $service->service_category_id }}">
                            {{ $service->category->category_name }}
                        </option>
                    @else
                        <option>Choose...</option>
                    @endif
                    @foreach ($categories as $category)
                        <option value="{{ $category->category_id }}">
                            {{ $category->category_name }}</option>
                    @endforeach

                </select>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
</x-Modal>
