<x-Layout :title="request()->segment(2)">

    <div class="row">
      
      @foreach ($users as $user)
        <x-Account.ImageCard :user="$user"></x-Account.ImageCard>


        <x-Account.Form 
        title="{{'Edit: '.' '.$user->login->login_email}}"
        submit_route="{{route('account-update',$user->account_id)}}"
        :towns="$towns" 
        :account="$user"
        modal="{{'edit'.$user->account_id}}">
      </x-Account.Form>

      {{-- Show Delete Modal --}}
      <x-DeleteModal 
        title="{{'Delete'.' '.$user->login->login_email}}"

        modal="{{'delete'.$user->account_id}}"
      
        delete_route="{{ route('account-delete',$user->account_id) }}">
      </x-DeleteModal>


      @endforeach
    </div>



  {{-- This shows the create frorm --}}
  <x-Account.Form title="Create"  submit_route='{{ route("account-store") }}'   :towns="$towns" modal="add-new"></x-Account.Form>

  <table class="table d-none" id="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Phone</th>
        <th scope="col">Email</th>
        <th scope="col">County Location</th>
        <th scope="col">Town Location</th>
        <th scope="col">Longitude</th>
        <th scope="col">Latitude</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($users as $i=>$user)
        
      <tr>
        <th scope="row">{{ $i+1 }}</th>
        <td>{{$user->account_first_name.' '.$user->account_last_name}}</td>
        <td>{{ $user->account_telephone_number}}</td>
        <td>{{$user->login->login_email}}</td>
        <td>{{$user->town->county->county_name  }}</td>
        <td>{{$user->town->town_name  }}</td>
        <td>{{$user->town->town_longitude  }}</td>
        <td>{{$user->town->town_latitude  }}</td>
      </tr>
      @endforeach
     
    </tbody>
  </table>


</x-Layout>