<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Login extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    public $timestamps = False;
    protected $table = 'login';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'login_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'login_email',
        'login_password',
        'login_rank',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'login_password',
    ];

   
    /**
     * Get the account associated with the login.
     */
    public function account()
    {
        return $this->belongsTo(Account::class, 'account_login_id', 'login_id');
    }
}



?>