<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;
    public $timestamps = False;


    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = "service_id";


    protected $fillable = [
        'service_title',
        'service_account_id',
        'service_category_id',
        'service_town_id'

    ];


    public function town()
    {
        return $this->belongsTo(Town::class, 'service_town_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'service_category_id');
    }

    public function account()
    {
        return $this->belongsTo(Account::class, 'service_account_id');
    }
}
