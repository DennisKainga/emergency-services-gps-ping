<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class County extends Model
{
    use HasFactory;
    public $timestamps = False;


    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'county_id';

    protected $fillable = [
        "county_name",
        "county_longitude",
        "county_latitude"
    ];


    public function towns()
    {
        return $this->hasMany(Town::class, 'town_county_id');
    }
}
