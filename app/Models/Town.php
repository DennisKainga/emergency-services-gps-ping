<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Town extends Model
{
    use HasFactory;
    public $timestamps = False;
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'town_id';

    protected $fillable = [

        "town_name",
        "town_longitude",
        "town_latitude",
        'town_county_id'


    ];


    public function accounts(){
        
        return $this->hasMany(Account::class, 'account_town_id');
    }

    public function county(){
        
        return $this->belongsTo(County::class, 'town_county_id');
    }


    public function garages(){

        return $this->hasMany(Garage::class, 'garage_town_id');
    }
}
