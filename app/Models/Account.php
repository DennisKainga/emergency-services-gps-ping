<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;
    public $timestamps = False;
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = "account_id";

    protected $fillable = [
        "account_first_name",
        "account_last_name",
        "account_telephone_number",
        "account_login_id",
        "account_town_id"
    ];



    public function login(){
        return $this->hasOne(Login::class, 'login_id', 'account_login_id');
    }


    public function town(){
        return $this->belongsTo(Town::class, 'account_town_id');
    }

    public function garages()
    {
        return $this->hasMany(Garage::class, 'garage_account_id');
    }
}
