<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Login;
use App\Models\Town;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index($rank)
    {
        $users = [];

        if ($rank == "customers") {
            $users = Account::whereHas('login', function ($query) {
                $query->where('login_rank', 1);
            })->get();
        } else if ($rank == 'supervisors') {
            $users = Account::whereHas('login', function ($query) {
                $query->where('login_rank', 0);
            })->get();
        } else {
            abort(403);
        }

        return view("accounts.index", [
            "users" => $users,
            "towns" => Town::all()
        ]);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $FormFields = $request->validate([
            "account_first_name" => ["required"],
            "account_last_name" => ["required"],
            "account_telephone_number" => ["required"],
            "login_email" => ["required", "unique:login,login_email"],
            "account_town" => ["required"],
            "login_rank" => ["required"]
        ]);

        Login::create([
            "login_email" => $request->login_email,
            "login_password" => Hash::make($request->login_email),
            "login_rank" => $request->login_rank
        ]);

        $user = User::create([
            'name' => $request->login_email,
            'email' => $request->login_email,
            'password' => Hash::make($request->login_email),
            'rank' => $request->login_rank
        ]);


        Account::create([

            "account_first_name" => $request->account_first_name,
            "account_last_name" => $request->account_last_name,
            "account_telephone_number" => $request->account_telephone_number,
            "account_login_id" => $user->id,
            "account_town_id" => $request->account_town

        ]);

        return redirect()->back()->with("success_message", "User added successfully");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Account $account, Request $request)
    {

        $FormFields = $request->validate([
            "account_first_name" => ["required"],
            "account_last_name" => ["required"],
            "account_telephone_number" => ["required"],
            "login_email" => ["required"],
            "account_town" => ["required"],

        ]);

        $account->update([
            "account_first_name" => $request->account_first_name,
            "account_last_name" => $request->account_last_name,
            "account_telephone_number" => $request->account_telephone_number,
            "account_town_id" => $request->account_town
        ]);
        $login = Login::where("login_id", $account->account_login_id)->first();

        $login->update([
            "login_email" => $request->login_email,
            "login_password" => Hash::make($request->login_email)
        ]);

        $user = User::where("id", $account->account_login_id)->first();

        $user->update([
            'name' => $request->login_email,
            'email' => $request->login_email,
            'password' => Hash::make($request->login_email)
        ]);

        return redirect()->back()->with("success_message", "User updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Account $account)
    {

        $account->login->delete();

        return redirect()->back()->with("info_message", "Account Deleted successfully");
        //
    }
}
