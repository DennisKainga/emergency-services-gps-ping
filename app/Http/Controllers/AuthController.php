<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller{

    public function index(){
        return view("auth.index");
    }

    public function authenticate(Request $request){
        $credentials = $request->only('email', 'password');
       
       
        if(auth()->attempt($credentials)){
            $user = Auth::user();
            if ($user->rank == 0 && Auth::guard('admin')->attempt($credentials)) {
                // Authentication passed for admin...
                return redirect()->route('dashboard')->with('success_message','Welcome');

            } else if ($user->rank == 1 && Auth::guard('customer')->attempt($credentials)) {
                // Authentication passed for customer...
                return redirect()->route('dashboard')->with('success_message','Welcome');
            } else {
                // Authentication failed...
                return redirect()->back()->with('error_message','Invalid login credentials.');
            }
        }
        
        
        else{
           
            // User not found in the database...
            return redirect()->back()->with('error_message','Invalid login credentials.');
        }

  
       
    }

    public function dashboard(){
        return view("dashboard.index");
    }

    public function logout(Request $request){

   
        Auth::logoutCurrentDevice();
        
    

        $request->session()->invalidate();

        $request->session()->regenerateToken();

    

        return redirect()->route('auth-index')->with('info_message','Good Bye !');
}
    //
}
