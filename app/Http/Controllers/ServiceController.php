<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Category;
use App\Models\Service;
use App\Models\Town;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        // 'service_title',
        // 'service_account_id',
        // 'service_category_id',
        // 'service_town_id'


        return view("services.index", [
            "services" => Service::all(),
            'towns' => Town::select("town_id", "town_name")->get(),
            'categories' => Category::select('category_id', 'category_name')->get(),
            'supervisors' => Account::select("account_id", "account_first_name", 'account_last_name')->whereHas('login', function ($query) {
                $query->where('login_rank', 0);
            })->get(),
        ]);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        // 'service_title',
        // 'service_account_id',
        // 'service_category_id',
        // 'service_town_id'
        // dd($request->service_category_id);
        $FormFields = $request->validate([
            "service_title" => ['required'],
            "service_account_id" => ["required"],
            'service_category_id' => ['required'],
            "service_town_id" => ['required']
        ]);

        Service::create([
            "service_title" => $request->service_title,
            'service_account_id' => $request->service_account_id,
            'service_category_id' => $request->service_category_id,
            'service_town_id' => $request->service_town_id
        ]);

        return redirect()->back()->with("success_message", "Service Added successfully");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }



    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Service $service)
    {
        // 'service_title',
        // 'service_account_id',
        // 'service_category_id',
        // 'service_town_id'

        $FormFields = $request->validate([
            "service_title" => ['required'],
            "service_account_id" => ["required"],
            'service_category_id' => ['required'],
            "service_town_id" => ['required']
        ]);

        $service->update([
            "service_title" => $request->service_title,
            'service_account_id' => $request->service_account_id,
            'service_category_id' => $request->service_category_id,
            'service_town_id' => $request->service_town_id
        ]);
        return redirect()->back()->with("success_message", "Service Updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Service $service)
    {
        $service->delete();

        return redirect()->back()->with("info_message", "Service Deleted successfully");
        //
    }
}
