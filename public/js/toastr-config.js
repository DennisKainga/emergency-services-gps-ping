$(document).ready(function() {
  const errorSound = new Audio('../sound/error.ogg');
  const successSound = new Audio('../sound/success.wav');
  const infoSound = new Audio('../sound/info.mp3');
  // Set toastr options
 
  toastr.options = {
    "closeButton": true,
    "debug": true,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "showDuration": "300",
    "hideDuration": "1000000",
    "timeOut": "10000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "slideDown",
    "hideMethod": "fadeOut"
  };
  
  if (session_data.success_message) {
    toastr.success(session_data.success_message, "Success!");
    successSound.play();
    delete session_data.success_message;
  }

  if (session_data.info_message) {
    toastr.info(session_data.info_message, "Info!");
    infoSound.play();
    delete session_data.success_message;
  }
  
  // Check for error message and display toast
  if (session_data.error_message) {
    toastr.error(session_data.error_message, "Error!");
    errorSound.play();
    delete session_data.success_message;
  }

  if (session_data.validation_errors) {
    session_data.validation_errors.forEach(function(error) {
      toastr["error"](error, "Validation Error!");
      errorSound.play();
    });
    delete session_data.validation_errors;
  }
});
